import java.util.Arrays;

public class Main {

	
	public static void main(String[] args){
		
		//Question 1
		String string1 = "ace";
		String string2 = "bdf";
		System.out.println(mergeSort(string1, string2));
		
		/*
		 * input 
		 * String string1 = "ace";
			String string2 = "bdf";
			
			output
			abcdef
		 */
		
		//Question 2
		System.out.println();
		System.out.println(findMinCharacter("akel"));
		
		/*
		 * Input: akel
		 * 
		 * output:  a
		 */
		
		//Question 3
		int[] array = { 1,20,30, 4, 50, 100,10};
		int op = binarySearch(array, 4);
		System.out.println(op);
		
		/*
		 * input: { 1,20,30, 4, 50, 100,10}
		 * output: 1
		 */
		
		//Question 4
		int[] array1 = { 1, 2, 3, 4, 5 };
		int sum = getSumOfArray(array1, array1.length-1);
		System.out.println(sum);
		
		/*
		 * input: { 1, 2, 3, 4, 5 }
		 * output: 15
		 */
		
		
		
		//Question 5
		CheckPalindrome("bib");
		
		/*
		 * input /output
		 *  input 1: bib
		 *  output: input is palindrome
		 *  
		 *  input 2: biba
		 *  output: input is not palindrome
		 */
	}
	
	//Question 1.
	static String mergeSort(String string1, String string2){
		return mergeSort(string1+string2);
	}
	
	static String mergeSort(String value){
		if(value.length()<2)
			return value;
		
		char min = value.charAt(0);
		for(int i = 1; i < value.length(); i++){
			char chr = value.charAt(i);
			if(chr < min)
				min = chr;
		}
		return String.valueOf(min) +mergeSort(value.replaceFirst(String.valueOf(min), "").trim());
	}
	
	//Question 2. 
	static String findMinCharacter(String value){
		
		if(value.length()<=1){
			return value;
		}
	
		if(value.charAt(0) == value.charAt(1)){
			return findMinCharacter(value.substring(1));
		}
		else if(value.charAt(0) > value.charAt(1)){
			return findMinCharacter(value.replace(String.valueOf(value.charAt(0)), "").trim());
		}
		else{
			return findMinCharacter(value.replace(String.valueOf(value.charAt(1)), "").trim());
		}
	}
	
	//Question 3.
	static int binarySearch(int[] arr, int item){
		Arrays.sort(arr);
		return binarySearch(arr, 0,arr.length-1,item);
	}
	
	public static int binarySearch(int[] a, int start, int end, int target) {
	    int middle = (start + end) / 2;
	    if(end < start) {
	        return -1;
	    } 

	    if(target==a[middle]) {
	        return middle;
	    } else if(target<a[middle]) {
	        return binarySearch(a, start, middle - 1, target);
	    } else {
	        return binarySearch(a, middle + 1, end, target);
	    }
	}
	
	//Question 4.
	static int getSumOfArray(int[] array, int n){
		if(n==0)
			return array[n];
		return array[n]+getSumOfArray(array, n-1);
	}

	//Question 5.
	static void CheckPalindrome(String input){
		String output = ReverseString(input);
		if(input.equals(output))
			System.out.println("input is palindrome");
		else
			System.out.println("input is not palindrome");
	}
		
	static String ReverseString(String value){
		if ((value==null)||(value. length() <= 1) )
			return value;
		
		return ReverseString(value. substring(1)) + value. charAt(0);
	}	
}